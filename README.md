# 2D-Optimizations
This project is developed by Unity 2019.4.20f1 LTS version. I recommend to use this version to avoid any error.
Project belongs to Trivuz®.

## Contributors
Md. Walid Bin Khalid Aunkon <mdwalidbinkhalidaunkon@gmail.com>

## License & Copyright
©Trivuz® 2017-2021 all right Reserved. Property of Trivuz®
Licensed under the [MIT License](LICENSE).